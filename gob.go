package main

import (
	"encoding/json"
	"log"
	"os"
	"text/template"
)

type class struct {
	Upper string `json: "upper"`
	Lower string `json: "lower"`
}

type namespace struct {
	Upper string `json: "upper"`
	Lower string `json: "lower"`
}

type function struct{}

type unit struct {
	Functions map[string]function `json: "functions"`
}

type Test struct {
	Class     class     `json: "class"`
	Namespace namespace `json: "namespace"`
	Unit      unit      `json: "unit"`
}

func main() {
	// Read JSON from stdin
	var test Test

	err := json.NewDecoder(os.Stdin).Decode(&test)
	if err != nil {
		log.Fatal(err)
	}

	// FIXME: don't care right now
	// Files are provided as a slice of strings.
	paths := []string{
		"/home/crdc/.templates/gob/glib-unit.tmpl",
	}

	t := template.Must(template.ParseFiles(paths...))
	err = t.Execute(os.Stdout, test)
	if err != nil {
		panic(err)
	}
}
