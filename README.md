# GObject Generator

Minimal utility to create GObject class implementations from templates and
JSON files as input for configuration.

## ToDo

* C
  * [ ] final class
  * [ ] derivable class
  * [ ] interface
  * [ ] unit test
* Vala
  * [ ] final class
  * [ ] derivable class
  * [ ] interface
  * [ ] unit test
* Python
  * [ ] final class
  * [ ] derivable class
  * [ ] interface
  * [ ] unit test
* Lua
  * [ ] final class
  * [ ] derivable class
  * [ ] interface
  * [ ] unit test
* Ruby
  * [ ] final class
  * [ ] derivable class
  * [ ] interface
  * [ ] unit test
* Rust
  * [ ] final class
  * [ ] derivable class
  * [ ] interface
  * [ ] unit test
